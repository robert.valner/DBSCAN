import os
import matplotlib.pyplot as plt
import argparse

def read_data(file_name):
    data_set_x = list()
    data_set_y = list()
    cluter_id = -1
    with open(file_name, 'r') as f:
        line = f.readline()
        while line:
            if '@@##@@'==line.strip():
                cluter_id += 1
                data_set_x.append(list())
                data_set_y.append(list())
                line = f.readline()
                continue
            items = line.split()
            data_set_x[cluter_id].append(float(items[0]))
            data_set_y[cluter_id].append(float(items[1]))
            line = f.readline()
    return data_set_x, data_set_y

def read_origin(file_name):
    data_set_x = list()
    data_set_y = list()
    with open(file_name, mode='r') as f:
        line = f.readline()
        while line:
            items = line.split()
            data_set_x.append(float(items[0]))
            data_set_y.append(float(items[1]))
            line = f.readline()
    return data_set_x, data_set_y


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--points', required=True, type=str, help='File containing all the points used for clustering')
    parser.add_argument('-c', '--clusters', required=True, type=str, help='File containing all detected clusters')
    args, unknown = parser.parse_known_args()

    file_name_all_points = args.points
    file_name_clusters = args.clusters

    data_set_origin_x, data_set_origin_y = read_origin(file_name_all_points)
    data_set_x, data_set_y = read_data(file_name_clusters)

    plt.figure(figsize=(1,2))
    plt.subplot(1,2,1)

    cmap = plt.cm.get_cmap('hsv', len(data_set_x))

    for i in range(len(data_set_x)):
        plt.plot(data_set_x[i], data_set_y[i], c=cmap(i), linestyle="", marker="o")

    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('dbscan')
    plt.subplot(1,2,2)
    plt.plot(data_set_origin_x, data_set_origin_y,'b*')

    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('origin')
    plt.show()

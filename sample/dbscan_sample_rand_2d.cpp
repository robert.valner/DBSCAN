/**
* \brief
* \author pengcheng (pengcheng@yslrpch@126.com)
* \date 2020-07-07
* \attention CopyrightÃ‚Â©ADC Technology(tianjin)Co.Ltd
* \attention Refer to COPYRIGHT.txt for complete terms of copyright notice
*/


#include <cstdlib>						// C standard library
#include <cstdio>						// C I/O (for sscanf)
#include <cstring>						// string manipulation
#include <fstream>	                    // file I/O
#include <iostream>
#include <vector>
#include <random>
#include <chrono>

#include "ANN/impl/dbscan.hpp"

int	main(int argc, char **argv)
{
    unsigned int nloops = 500;
    unsigned int min_duration = 100000;
    unsigned int max_duration = 0;
    unsigned int mean_duration = 0;

    std::vector<std::vector<double>> pts;

    for (size_t n=0; n<nloops; n++)
    {
        //Stuff some more points in.
        size_t FixedSeed = std::chrono::milliseconds(std::chrono::seconds(std::time(NULL))).count();
        std::mt19937 re(FixedSeed);

        std::uniform_real_distribution<> rd(-100.0, 100.0);
        for(size_t i = 0; i < 250; ++i) pts.push_back(std::vector<double>{rd(re), rd(re)});

        std::uniform_real_distribution<> rd_2(-55.0, 5.0);
        for(size_t i = 0; i < 125; ++i) pts.push_back(std::vector<double>{rd_2(re), rd_2(re)});

        std::uniform_real_distribution<> rd_3(-5.0, 55.0);
        for(size_t i = 0; i < 125; ++i) pts.push_back(std::vector<double>{rd_3(re), rd_3(re)});

        auto start = std::chrono::steady_clock::now();
        dbscan::Dbscan<double> scan = dbscan::Dbscan<double>(2, 90, 6);
        scan.Run(pts);
        auto end = std::chrono::steady_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
        mean_duration += duration;

        if (duration < min_duration)
        {
            min_duration = duration;
        }

        if (duration > max_duration)
        {
            max_duration = duration;
        }

        if (true && n == (nloops-1)) // Generate plot
        {
            std::cout << "Generating a data plot" << std::endl;

            std::vector<std::vector<std::vector<double>>> res;
            scan.GetCluster(res);
            std::fstream data_out;
            data_out.open("../dataset/dbscan-res.pts", std::ios::out);

            for(std::vector<std::vector<double>> cluster : res)
            {
                data_out<<"@@##@@"<<std::endl;
                for(std::vector<double> pt : cluster)
                {
                    data_out<<pt[0]<<" "<<pt[1]<<std::endl;
                }
            }

            std::fstream data_out_orig;
            data_out_orig.open("../dataset/dbscan-input.pts", std::ios::out);
            for(std::vector<double> pt : pts)
            {
                data_out_orig<<pt[0]<<" "<<pt[1]<<std::endl;
            }

            data_out.close();
            data_out_orig.close();
        }

        pts.clear();
    }

    mean_duration = mean_duration/nloops;
    std::cout << "Iterations: " << nloops
        << " | Mean: " << mean_duration
        << " ms | Max: " << max_duration
        << " ms | Min: " << min_duration << " ms" << std::endl;

   // system("pause");
    return 0;
}
